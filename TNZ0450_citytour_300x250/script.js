var fadeTime = 2;
var animTimeIn = 1;
var animTimeOut = 1;
var staggerOffset = 0.25;
var mapMarkerAnimTime = 2;
var animOffsetDist = 30;
var imageScaleTime = 6;
var imageStartScale = 1.2;
var bookmarkBaseAlpha = 0.75;

var animTypeIn = Cubic.easeOut;
var animTypeOut = Cubic.easeOut;
var imageScaleAnimType = Cubic.easeInOut;
var mapMarkerAnimType = Elastic.easeOut.config(1, 0.25);

var bgimage1 = document.getElementById('bgimage1');
var bookmarkBase = document.getElementById('bookmark-base');
var headline1 = document.getElementById('headline1');
var map = document.getElementById('map');
var mapmarker1 = document.getElementById('mapmarker1');
var location1 = document.getElementById('location1');
var bookmarkDivider = document.getElementById('bookmark-divider');
var pnzSmall = document.getElementById('pnz-small');

var partnerLogo = document.getElementById('partner-logo');
var offer = document.getElementById('offer');
var price = document.getElementById('price');
var btnCTA = document.getElementById('btn-cta');
var pnzLarge = document.getElementById('pnz-large');
var terms = document.getElementById('terms');

var blackOverlay = document.getElementById('black-overlay');

function initBanner() {
    console.log('initBanner');
    addEventListeners();
    animateBanner();
}

function animateBanner() {
    console.log('animateBanner');
    frame1();
}

function bgExitHandler(event) {
    console.log('bgExitHandler');
    Enabler.exit('Exit');
}

function addEventListeners() {
    console.log('addEventListeners');
    document.getElementById('bgexit').addEventListener('click', bgExitHandler, false);
    document.getElementById('btn-cta').addEventListener('click', bgExitHandler, false);
}

function frame1() {
    console.log('frame1');

    bgimage1.style.visibility = 'visible';

    TweenMax.to(blackOverlay, fadeTime, {
        autoAlpha: 0,
        ease: animTypeIn
    });

    TweenMax.to(bgimage1, imageScaleTime, {
        scale: 1 / imageStartScale,
        ease: imageScaleAnimType
    });

    TweenMax.from(headline1, animTimeIn, {
        delay: fadeTime,
        y: '-=' + animOffsetDist,
        autoAlpha: 0,
        ease: animTypeIn
    });

    TweenMax.from([bookmarkBase, map, location1, bookmarkDivider, pnzSmall], animTimeIn, {
        delay: fadeTime,
        y: '+=' + animOffsetDist,
        autoAlpha: 0,
        ease: animTypeIn
    });

    TweenMax.from(mapmarker1, mapMarkerAnimTime, {
        delay: fadeTime + animTimeIn,
        scale: 0,
        autoAlpha: 0,
        ease: mapMarkerAnimType
    });

    TweenMax.delayedCall(8, frame2);

}




function frame2() {

    console.log('frame2');

    TweenMax.to(bookmarkBase, animTimeOut + animTimeIn, {
        x: -103,
        y: -192,
        width: 300 + 20,
        height: 250 + 20,
        alpha: bookmarkBaseAlpha,
        ease: Cubic.easeInOut
    });

    // frame 1 assets out

    TweenMax.to([headline1, map, mapmarker1, location1, bookmarkDivider, pnzSmall], animTimeOut, {
        autoAlpha: 0,
        ease: animTypeOut
    });


    // frame 2 assets in

    TweenMax.staggerFrom([partnerLogo, offer, price, btnCTA, pnzLarge], animTimeIn, {
        delay: animTimeOut,
        y: '+=' + animOffsetDist,
        autoAlpha: 0,
        ease: animTypeIn
    }, staggerOffset);

    TweenMax.to(terms, animTimeIn, {
        delay: 3,
        autoAlpha: 1,
        ease: animTypeIn
    });


}


initBanner();